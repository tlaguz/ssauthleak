﻿using NUnit.Framework;
using ServiceStack;

namespace AuthLeak
{
    [TestFixture]
    internal class ATest
    {
        private Program.AppHost _appHost;

        [SetUp]
        public void SetUp()
        {
            _appHost = new Program.AppHost();
            _appHost.LoadPlugin(new AuthNeededTestPlugin());
            _appHost.Init().Start(Program.AppHost.TestUrl);
        }

        [Test]
        public void Test()
        {
            var client = new JsonServiceClient(Program.AppHost.TestUrl);
            var result = client.Get(new DoIHaveSession());
            Assert.True(result.Result);
        }

        [TearDown]
        public void TearDown()
        {
            _appHost.Dispose();
            _appHost = null;
        }
    }
}
