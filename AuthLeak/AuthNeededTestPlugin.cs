﻿using ServiceStack;
using ServiceStack.Auth;
using System;
using System.Collections.Generic;

namespace AuthLeak
{
    internal class AuthNeededTestPlugin : IPlugin
    {
        public void Register(IAppHost appHost)
        {
            appHost.LoadPlugin(new AuthFeature(
                () => new AuthSession(),
                new IAuthProvider[]
                {
                    new CredentialsAuthProvider()
                }
                )
            {
                HtmlRedirect = null,
                ServiceRoutes = new Dictionary<Type, string[]>
                {
                    {typeof(AuthenticateService), new[] {"/auth", "/auth/{provider}"}}
                }
            });
        }
    }
}
