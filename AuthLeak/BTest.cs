﻿using NUnit.Framework;
using ServiceStack;

namespace AuthLeak
{
    [TestFixture]
    internal class BTest
    {
        private Program.AppHost _appHost;

        [SetUp]
        public void SetUp()
        {
            _appHost = new Program.AppHost();
            _appHost.Init().Start(Program.AppHost.TestUrl);
        }

        [Test]
        public void Test()
        {
            var client = new JsonServiceClient(Program.AppHost.TestUrl);
            var result = client.Get(new DoIHaveSession());
            Assert.False(result.Result);
        }

        [TearDown]
        public void TearDown()
        {
            _appHost.Dispose();
            _appHost = null;
        }
    }
}
