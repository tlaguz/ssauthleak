﻿using ServiceStack;
using System;

namespace AuthLeak
{
    internal class Program
    {
        //Define the Web Services AppHost
        public class AppHost : AppSelfHostBase
        {
            public static string TestUrl = "http://localhost:1337/";

            public AppHost()
                : base("HttpListener Self-Host", typeof(Program).Assembly) { }

            public override void Configure(Funq.Container container) { }
        }

        //Run it!
        private static void Main(string[] args)
        {
            var listeningOn = args.Length == 0 ? "http://localhost:1337/" : args[0];
            var appHost = new AppHost()
                .Init()
                .Start(listeningOn);

            Console.WriteLine("AppHost Created at {0}, listening on {1}",
                DateTime.Now, listeningOn);

            Console.ReadKey();
        }
    }
}