﻿using ServiceStack;

namespace AuthLeak
{
    [Route("/doIHaveSession")]
    public class DoIHaveSession : IReturn<DoIHaveSessionResponse>
    {
    }

    public class DoIHaveSessionResponse
    {
        public bool Result { get; set; }
    }

    public class TestingService : Service
    {
        public object Any(DoIHaveSession dto)
        {
            var session = GetSession() as AuthSession;
            return new DoIHaveSessionResponse { Result = session != null };
        }
    }
}
